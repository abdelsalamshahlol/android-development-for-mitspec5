package model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by c0mm4nd3r on 2/18/18.
 */

public class Response {
    private List<News> articles = new ArrayList<News>();

    public List<News> getArticles() {
        return articles;
    }
}
