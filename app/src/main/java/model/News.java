package model;

/**
 * Created by c0mm4nd3r on 2/18/18.
 */

public class News {
    private String title;
    private String description;
    private String url;

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getUrl() {
        return url;
    }
}
