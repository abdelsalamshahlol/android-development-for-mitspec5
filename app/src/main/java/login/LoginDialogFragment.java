package login;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aashahlol.act2.R;

/**
 * Created by c0mm4nd3r on 2/18/18.
 */

//custom class to create a Dialog
public class LoginDialogFragment extends DialogFragment {

    String user = "abdel";
    String pass = "1234";
    String entered_user;
    String entered_pass;
    EditText user_et;
    EditText pass_et;

    /* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */
    public interface NoticeDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog);

        public void onDialogNegativeClick(DialogFragment dialog);
    }

    // Use this instance of the interface to deliver action events
    NoticeDialogListener mListener;

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (NoticeDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog, null);

        builder.setTitle(R.string.dialog_title)
//                .setView(inflater.inflate(R.layout.custom_dialog, null))
                .setView(dialogView)
                .setPositiveButton(R.string.login, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.onDialogPositiveClick(LoginDialogFragment.this);
                        user_et = dialogView.findViewById(R.id.user_name);
                        pass_et = dialogView.findViewById(R.id.user_pass);
                        entered_user = String.valueOf(user_et.getText());
                        entered_pass = String.valueOf(pass_et.getText());

                        if (entered_user.equals(user) && entered_pass.equals(pass)) {
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            View layout = inflater.inflate(R.layout.correct,
                                    (ViewGroup) getActivity().findViewById(R.id.toast_c_layout));
                            Toast t = new Toast(getContext());
                            t.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER, 0, 0);
                            TextView text = (TextView) layout.findViewById(R.id.messageView);
                            text.setText("Welcome Back Salam");
                            t.setView(layout);
                            t.show();
                        } else if (entered_user.isEmpty() || entered_pass.isEmpty()) {
                            Toast.makeText(getContext(), "Enter Username And Password", Toast.LENGTH_SHORT).show();
                        } else {
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            View layout = inflater.inflate(R.layout.incorrect,
                                    (ViewGroup) getActivity().findViewById(R.id.toast_in_layout));
                            Toast t = new Toast(getContext());
                            t.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER, 0, 0);
                            TextView text = (TextView) layout.findViewById(R.id.messageView);
                            text.setText("Wrong Username Or Password");
                            t.setView(layout);
                            t.show();
//                            Toast.makeText(getContext(), "Wrong Username Or Password", Toast.LENGTH_SHORT).show();
                        }

                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        LoginDialogFragment.this.getDialog().cancel();
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }
}
