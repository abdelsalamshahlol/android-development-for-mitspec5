package com.example.aashahlol.act2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Calculator extends AppCompatActivity {
    TextView tv;
    EditText et1;
    EditText et2;
    double result=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);
        tv = findViewById(R.id.result);
        et1 =  findViewById(R.id.num1);
        et2 =findViewById(R.id.num2);

    }

    public void doAction(View v) {
        String num1 = et1.getText().toString();
        String num2 = et2.getText().toString();
        double num1F=0;
        double num2F=0;
        try {
             num1F =Double.parseDouble(num1);
             num2F =Double.parseDouble(num2);
        }catch (Exception e){
            Toast.makeText(this,"Error Message: "+e.getMessage(),Toast.LENGTH_LONG).show();
        }


        switch (v.getId()) {
            case (R.id.add):
                tv.setText(String.valueOf(num1F+num2F));
                break;
            case (R.id.sub):
                tv.setText(String.valueOf(num1F-num2F));
                break;

            case (R.id.mp):
                tv.setText(String.valueOf(num1F*num2F));
                break;

            case (R.id.dv):
                try {
                    result = num1F/num2F;
                    tv.setText(String.valueOf(result));
                }
                catch (Exception e){
                    Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();
                }
                break;
        }
    }
}
