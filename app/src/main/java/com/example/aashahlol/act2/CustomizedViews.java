package com.example.aashahlol.act2;

import android.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import login.LoginDialogFragment;
import model.LoadJSONTask;
import model.News;


public class CustomizedViews  extends AppCompatActivity implements LoadJSONTask.Listener,AdapterView.OnItemClickListener,LoginDialogFragment.NoticeDialogListener  {
    ListView lv;
    //    ArrayAdapter adapter;
    ArrayList myStringArray = new ArrayList<>(Arrays.asList("Sony XZ", "Samsung S8+", "OnePlus 5", "Pixel XL"));
    ListAdapter adapter;
    EditText user_et;
    EditText pass_et;

    public static final String URL = "https://newsapi.org/v2/top-headlines?country=ph&apiKey=7b62a7a52042410a83cb78113dff1c52";

    private List<HashMap<String, String>> mNewsMapList = new ArrayList<>();

    private static final String KEY_Title = "title";
    private static final String KEY_Description= "description";
    private static final String KEY_URL = "url";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom);
        lv = findViewById(R.id.lv);
//        user_et = findViewById(R.id.user_name);
//        pass_et = findViewById(R.id.user_pass);
//        adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, myStringArray);
//        lv.setAdapter(adapter);
        lv.setOnItemClickListener(this);
        registerForContextMenu(this.lv);
        new LoadJSONTask(this).execute(URL);
    }



    //this method used to create the context menu called confirm
    @Override
    public void onCreateContextMenu(ContextMenu confirm, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(confirm, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.confirm, confirm);
    }

    //this is for the delete action
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int listPos = info.position;
        try{
            mNewsMapList.remove(listPos);
//            adapter.notifyDataSetChanged();
            ((BaseAdapter)adapter).notifyDataSetChanged();
        }
        catch (Exception e){
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
        return true;
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        switch (i) {
            case 0:
//                Toast.makeText(this, "You have clicked "+myStringArray.get(i), Toast.LENGTH_SHORT).show();
                Toast.makeText(this, "You have clicked "+mNewsMapList.get(i).get(KEY_Title), Toast.LENGTH_SHORT).show();
                break;
            case 1:
                Toast.makeText(this, "You have clicked "+mNewsMapList.get(i).get(KEY_Title), Toast.LENGTH_SHORT).show();
                break;
            case 2:
                Toast.makeText(this, "You have clicked "+mNewsMapList.get(i).get(KEY_Title), Toast.LENGTH_SHORT).show();
                break;
            case 3:
                Toast.makeText(this, "You have clicked "+mNewsMapList.get(i).get(KEY_Title), Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(this, "Sorry not available", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public void doLogin(View v){
        LoginDialogFragment l = new LoginDialogFragment();
        l.show(getFragmentManager(),"Dialog");
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        // User touched the dialog's positive button
//        String user="Salam";
//        String pass="1234";
//        String entered_user;
//        String entered_pass;
//
//
//
//        entered_user = String.valueOf(user_et.getText());
//        entered_pass = String.valueOf(pass_et.getText());
//
//        if(entered_user.equals(user) && entered_pass.equals(pass)){
//            Toast.makeText(this,"Welcome Back Salam",Toast.LENGTH_SHORT).show();
//        }
//        else{
//            Toast.makeText(this,"Wrong Username Or Password",Toast.LENGTH_SHORT).show();
//        }
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        // User touched the dialog's negative button

    }

    @Override
    public void onLoaded(List<News> newsList) {
        for (News articles : newsList) {

            HashMap<String, String> map = new HashMap<>();

            map.put(KEY_Title, articles.getTitle());
            map.put(KEY_Description, articles.getUrl());
            map.put(KEY_URL, articles.getDescription());

            mNewsMapList.add(map);
        }

        loadListView();
    }

    @Override
    public void onError() {

        Toast.makeText(this, "Error !", Toast.LENGTH_SHORT).show();
    }

    private void loadListView() {

        adapter = new SimpleAdapter(CustomizedViews.this, mNewsMapList, R.layout.lv,
                new String[] { KEY_Title, KEY_Description , KEY_URL},
                new int[] { R.id.title, R.id.desc,R.id.url});

        lv.setAdapter(adapter);

    }

}
