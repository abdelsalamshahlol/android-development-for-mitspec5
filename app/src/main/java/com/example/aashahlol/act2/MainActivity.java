package com.example.aashahlol.act2;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{
    ListView lv;
    ArrayAdapter adapter;
    String [] myStringArray= {"Basic Calculator","Layout","Input Control","Context Menu, Customized Dialog, and Toast","Fragments"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lv =  findViewById(R.id.listView);
        adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1, myStringArray);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent innt;
        switch (i){
            case 0:
               innt = new Intent(this,Calculator.class);
                startActivity(innt);
                break;
            case 1:
                 innt = new Intent(this,ProfileActivity.class);
                startActivity(innt);
//                Toast.makeText(this, myStringArray[i], Toast.LENGTH_LONG).show();
                break;
            case 2:
                innt = new Intent(this,InputActivity.class);
                startActivity(innt);
                break;
            case 3:
                innt = new Intent(this,CustomizedViews.class);
                startActivity(innt);
                break;
            case 4:
                innt = new Intent(this,FragmentActivity.class);
                startActivity(innt);
                break;
            default:
                Toast.makeText(this, "Sorry not available", Toast.LENGTH_LONG).show();
                break;
        }
    }
}
