package com.example.aashahlol.act2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class InputActivity extends AppCompatActivity {
    EditText name;
    String gender;
    RadioButton gen_f, gen_m;
    CheckBox hb1, hb2, hb3, hb4, hb5, hb6;
    ArrayList<String> hobbies = new ArrayList<String>();
    private Intent innt;
    int genId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);
        name = findViewById(R.id.name);
//        gender = findViewById(R.id.gender);
//        genId = gender.getCheckedRadioButtonId();
//        genderSelected = findViewById(genId);
        gen_f = findViewById(R.id.gen_f);
        gen_m = findViewById(R.id.gen_m);
        hb1 = findViewById(R.id.hb1);
        hb2 = findViewById(R.id.hb2);
        hb3 = findViewById(R.id.hb3);
        hb4 = findViewById(R.id.hb4);
        hb5 = findViewById(R.id.hb5);
        hb6 = findViewById(R.id.hb6);
    }

    public void doAction(View v) {
//        Toast.makeText(this, "Results " + hobbies.toString(), Toast.LENGTH_LONG).show();
        innt = new Intent(this, InputResult.class);
        innt.putExtra("hobbies", hobbies.toString());
        innt.putExtra("name", name.getText().toString());
        innt.putExtra("gender",gender);

        startActivity(innt);
    }

    public void onRadioButtonClicked(View v) {
        boolean checked = ((RadioButton) v).isChecked();
        switch (v.getId()) {
            case R.id.gen_f:
                if (checked)
                    gender = gen_f.getText().toString();
                break;

            case R.id.gen_m:
                if (checked)
                    gender = gen_m.getText().toString();
                break;

            default:
                gender = "Unchecked";
                break;
        }
    }

    public void onCheckboxClicked(View v) {

        boolean checked = ((CheckBox) v).isChecked();
        switch (v.getId()) {
            case R.id.hb1:
                if (checked) {
                    hobbies.add(hb1.getText().toString());
                } else {
                    hobbies.remove(hb1.getText().toString());
                }
                break;
            case R.id.hb2:
                if (checked) {
                    hobbies.add(hb2.getText().toString());
                } else {
                    hobbies.remove(hb2.getText().toString());
                }
                break;
            case R.id.hb3:
                if (checked) {
                    hobbies.add(hb3.getText().toString());
                } else {
                    hobbies.remove(hb3.getText().toString());
                }
                break;
            case R.id.hb4:
                if (checked) {
                    hobbies.add(hb4.getText().toString());
                } else {
                    hobbies.remove(hb4.getText().toString());
                }
                break;
            case R.id.hb5:
                if (checked) {
                    hobbies.add(hb5.getText().toString());
                } else {
                    hobbies.remove(hb5.getText().toString());
                }
                break;
            case R.id.hb6:
                if (checked) {
                    hobbies.add(hb6.getText().toString());
                } else {
                    hobbies.remove(hb6.getText().toString());
                }
                break;
        }
    }
}