package com.example.aashahlol.act2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class InputResult extends AppCompatActivity {
TextView result_n;
TextView result_g;
TextView result_h;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_result);
        result_n = findViewById(R.id.result_n);
        result_g = findViewById(R.id.result_g);
        result_h = findViewById(R.id.result_h);
        Bundle extras = getIntent().getExtras();
        if (extras != null)
        {
            String resName = extras.getString("name");
            String resGender = extras.getString("gender");
            String resHobbies = extras.getString("hobbies");

            result_n.setText(resName);
            result_g.setText(resGender);
            result_h.setText(resHobbies);
        }
        else
        {
            Toast.makeText(this,"NOTHING !!",Toast.LENGTH_LONG).show();

        }
    }
}
