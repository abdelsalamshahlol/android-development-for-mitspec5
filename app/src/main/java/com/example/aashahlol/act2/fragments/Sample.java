package com.example.aashahlol.act2.fragments;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.aashahlol.act2.R;

public class Sample  extends Fragment {
    Button btn;
    EditText et;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
//        return super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.sample, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final  TextView tv = getActivity().findViewById(R.id.result);
        btn = getView().findViewById(R.id.btn);
        et = getView().findViewById(R.id.yourText);
        btn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                String t = String.valueOf(et.getText());
                tv.setText(t);
            }
        });
    }
}